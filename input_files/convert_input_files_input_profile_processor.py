"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
This script inspects the input files and modifies them such that they adhere to the new input file specification used by the framework after merge of merge request !13 Input Profile Processor.
This script assumes that the file containing the paths to the files containing the input profiles is called "data_path.csv".
"""

import os.path

changed_files = []
for dirpath, dirnames, filenames in os.walk(".\\input_files"):
    for filename in filenames:
        if filename == "data_path.csv":
            print(f"Inspecting file {os.path.join(dirpath, filename)}")
            changed_files.append(os.path.join(dirpath, filename))
            os.remove(os.path.join(dirpath, filename))
for file in changed_files:
    print(f"Modified file {file}!")
