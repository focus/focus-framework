"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
This script inspects the input files and modifies them such that they adhere to the new input file specification used by the framework after merge of merge request !16 Refactoring Part 2.
"""

import os.path
import pandas as pd
import math

def read_matrix(df):
    matrix = []
    for i in df.index:
        matrix_row = []
        for j in df.index:
            matrix_row.append(df[df["name"][j]][i])
        matrix.append(matrix_row)
    return matrix

def read_components(df):
    components = {}
    for i in df.index:
        components[df["name"][i]] = i
    return components

def compare_components(comp_1, comp_2):
    different = False
    for column in ['name', 'type', 'model', 'min_size', 'max_size', 'current_size']:
        if comp_1[column] != comp_2[column]:
            if not math.isnan(comp_1[column]) and not math.isnan(comp_2[column]):
                different = True
                break
    return different

def get_connected_components(matrix, components, comp):
    connected_components = []
    for component in components:
        if get_connection(matrix, components, comp, component) == 1:
            connected_components.append(component)
    return connected_components

def get_connection(matrix, components, comp_from, comp_to):
    return matrix[components[comp_from]][components[comp_to]]

def get_bypasses(component_name, connections):
    inputs = []
    outputs = []
    sector = ''
    for i in connections.index:
        if connections['to'][i] == component_name and connections['from'][i] not in inputs:
            inputs.append(connections['from'][i])
            if sector == '':
                sector = connections['sector'][i]
            else:
                if sector != connections['sector'][i]:
                    raise KeyError
        if connections['from'][i] == component_name and connections['to'][i] not in outputs:
            outputs.append(connections['to'][i])
            if sector == '':
                sector = connections['sector'][i]
            else:
                if sector != connections['sector'][i]:
                    raise KeyError
    bypasses = []
    for input in inputs:
        for output in outputs:
            if input != output:
                if not ((connections['sector'] == sector) & (connections['from'] == input) & (connections['to'] == output)).any():
                    bypasses.append((sector, input, output))
    return bypasses

def read_config(config_path):
    config_df = pd.read_csv(config_path)
    config_dict = config_df.to_dict(orient='list')

    for k in config_dict:
        config_dict[k] = config_dict[k][0]
    return config_dict

changed_topologies = []
invalid_topologies = []
renamed_components = {'StandardElectricalConsumption': 'ElectricalConsumption',
                      'StandardACGrid': 'ElectricalGrid',
                      'StandardPVGenerator': 'PVGenerator',
                      'StandardGasGrid': 'GasGrid',
                      'StandardPEMElectrolyzer': 'PEMElectrolyzer',
                      'StandardPEMFuelCell': 'PEMFuelCell',
                      'BasicInverter': 'StaticInverter',
                      'Inverter': 'DynamicInverter'}
consumption_components = ['CoolConsumption', 'ChargingInfrastructure', 'ElectricalConsumption', 'HeatConsumption', 'Radiator', 'HotWaterConsumption']
storage_components = ['LiionBattery', 'HotWaterStorage', 'PressureStorage']
for dirpath, dirnames, filenames in os.walk(".\\input_files"):
    topology_here = False
    matrix_files = []
    for filename in filenames:
        if filename.find('matrix') and filename.endswith('.csv') and (filename.find('elec') is not -1):
            topology_here = True
            matrix_files.append((filename, 'electricity'))
        if filename.find('matrix') and filename.endswith('.csv') and (filename.find('gas') is not -1):
            topology_here = True
            matrix_files.append((filename, 'gas'))
        if filename.find('matrix') and filename.endswith('.csv') and (filename.find('hydro') is not -1):
            topology_here = True
            matrix_files.append((filename, 'hydrogen'))
        if filename.find('matrix') and filename.endswith('.csv') and (filename.find('therm') is not -1):
            topology_here = True
            matrix_files.append((filename, 'heat'))
    if topology_here:
        try:
            print(f"Inspecting topology {dirpath}")
            file_contents = []
            for matrix_file, sector in matrix_files:
                df = pd.read_csv(os.path.join(dirpath, matrix_file))
                df.rename(columns = {'comp_name': 'name', 'comp_type': 'type'}, inplace = True)
                for i in df.index:
                    if df['type'][i] in renamed_components:
                        df.loc[i, 'type'] = renamed_components[df['type'][i]]
                    if df['type'][i] in consumption_components:
                        df.loc[i, 'model'] = ''
                        df.loc[i, 'min_size'] = ''
                        df.loc[i, 'max_size'] = ''
                        df.loc[i, 'current_size'] = ''
                file_contents.append((df, read_matrix(df), read_components(df), sector))
            all_components = pd.DataFrame(columns = ['name', 'type', 'model', 'min_size', 'max_size', 'current_size'])
            all_connections = pd.DataFrame(columns = ['sector', 'from', 'to'])
            for df, matrix, components, sector in file_contents:
                for component in components:
                    if component not in all_components.loc[:]['name']:
                        all_components.loc[component] = df.loc[components[component]][['name', 'type', 'model', 'min_size', 'max_size', 'current_size']]
                    else:
                        if compare_components(all_components.loc[component], df.loc[components[component]][['name', 'type', 'model', 'min_size', 'max_size', 'current_size']]):
                            raise KeyError
                    for connected_component in get_connected_components(matrix, components, component):
                        all_connections = pd.concat([all_connections, pd.Series({'sector': sector, 'from': component, 'to': connected_component}).to_frame().T], ignore_index = True)
            while True:
                number_of_connections_before = len(all_connections)
                for i in all_components.index:
                    if all_components['type'][i] in storage_components:
                        for sector, from_component, to_component in get_bypasses(all_components['name'][i], all_connections):
                            all_connections = pd.concat([all_connections, pd.Series({'sector': sector, 'from': from_component, 'to': to_component}).to_frame().T], ignore_index = True)
                number_of_connections_after = len(all_connections)
                if number_of_connections_before == number_of_connections_after:
                    break
            
            config = read_config(os.path.join(dirpath, "config.csv"))
            changed_topologies.append(dirpath)
            new_config = dict()
            for name, value in config.items():
                # configs to delete
                if name in ['injection/pvpeak', 'elec_price_cap_low', 'elec_price_cap_high']:
                    pass
                # configs to keep
                elif name in ['yearly_interest', 'planning_horizon']:
                    new_config[name] = value
                # configs that are handled by other configs
                elif name in ['elec_price_variable', 'injection_price_variable', 'gas_price_variable', 'injection_price_gas_variable', 'heat_price_variable', 'injection_price_heat_variable', 'cooling_price_variable', 'injection_price_cooling_variable']:
                    pass
                elif name == 'elec_price':
                    if config['elec_price_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable electricity price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'ElectricalGrid':
                                new_config[all_components['name'][i] + '_price'] = value
                elif name == 'injection_price':
                    if config['injection_price_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable electricity injection price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'ElectricalGrid':
                                new_config[all_components['name'][i] + '_injection_price'] = value
                elif name == 'gas_price':
                    if config['gas_price_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable gas price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'GasGrid':
                                new_config[all_components['name'][i] + '_price'] = value
                elif name == 'injection_price_gas':
                    if config['injection_price_gas_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable gas injection price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'GasGrid':
                                new_config[all_components['name'][i] + '_injection_price'] = value
                elif name == 'heat_price':
                    if config['heat_price_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable heat price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'HeatGrid':
                                new_config[all_components['name'][i] + '_price'] = value
                elif name == 'injection_price_heat':
                    if config['injection_price_heat_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable heat injection price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'HeatGrid':
                                new_config[all_components['name'][i] + '_injection_price'] = value
                elif name == 'cooling_price':
                    if config['cooling_price_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable cooling price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'CoolingGrid':
                                new_config[all_components['name'][i] + '_price'] = value
                elif name == 'injection_price_cooling':
                    if config['injection_price_cooling_variable'] == 1:
                        print(f"Config of topology {dirpath} has a variable cooling injection price, so be sure to add a price profile!")
                    else:
                        for i in all_components.index:
                            if all_components['type'][i] == 'CoolingGrid':
                                new_config[all_components['name'][i] + '_injection_price'] = value
                elif name == 'elec_emission':
                    for i in all_components.index:
                        if all_components['type'][i] == 'ElectricalGrid':
                            new_config[all_components['name'][i] + '_emission'] = value
                elif name == 'gas_emission':
                    for i in all_components.index:
                        if all_components['type'][i] == 'GasGrid':
                            new_config[all_components['name'][i] + '_emission'] = value
                else:
                    new_config[name] = value
            all_components.to_csv(os.path.join(dirpath, "temp.csv"), index = False)
            df_str = pd.read_csv(os.path.join(dirpath, "temp.csv"), dtype = str, keep_default_na = False)
            for i in all_components.columns:
                counter = 0
                for j in all_components.index:
                    if issubclass(type(all_components[i][j]), float):
                        if len(df_str[i][counter]) != 0 and df_str[i][counter].endswith(".0"):
                            df_str[i][counter] = f"{all_components[i][j]:.0f}"
                    counter += 1
            df_str.to_csv(os.path.join(dirpath, "components.csv"), index = False)
            os.remove(os.path.join(dirpath, "temp.csv"))
            all_connections.to_csv(os.path.join(dirpath, "connections.csv"), index = False)
            for matrix_file, sector in matrix_files:
                os.remove(os.path.join(dirpath, matrix_file))
            os.remove(os.path.join(dirpath, "config.csv"))
            new_config_df = pd.DataFrame(columns = list(new_config.keys()), index = [0])
            for key, value in new_config.items():
                string = str(value)
                if issubclass(type(value), float):
                    if len(string) != 0 and string.endswith(".0"):
                        string = f"{value:.0f}"
                new_config_df.loc[0, key] = string
            new_config_df.to_csv(os.path.join(dirpath, "config.csv"), index = False)
        except:
            invalid_topologies.append(dirpath)
for directory in changed_topologies:
    print(f"Modified topology {directory}!")
for file in invalid_topologies:
    print(f"Topology {file} breaks some part of the input file specification!")
