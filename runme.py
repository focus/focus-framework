"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import pandas as pd
import Tooling.input_profile_processor.input_profile_processor
from Tooling.dynamics.Dynamic import GlobalDynamic
import Model_Library.Prosumer.main as main_prosumer
import Model_Library.District.main as main_district
from enum import Enum
import json

class SimulationScope(Enum):
    PROSUMER = 1
    DISTRICT = 2

simulation_scope = SimulationScope.DISTRICT
t_start = pd.Timestamp("2019-05-10 00:00:00") # start time of simulation
global_dynamic = GlobalDynamic([3600 for i in range(240)])
dynamic = global_dynamic.root()

input_profile_dict = {'irradiance_1': {'type': 'irradiance', 'file': 'input_files/data/irradiance/Lindenberg2006BSRN_Irradiance_60sec.csv'},
                      'temperature_1': {'type': 'air_temperature', 'file': 'input_files/data/temperature/temperature.csv'},
                      'demand_electric_1': {'type': 'elec_demand', 'generate': {'yearly_demand': 3000}},
                      'demand_heat_1': {'type': 'therm_demand', 'generate': {'yearly_demand': 6000, 'temperature': 'temperature_1'}},
                      'demand_hot_water_1': {'type': 'hot_water_demand', 'generate': {'yearly_demand': 1500, 'temperature': 'temperature_1'}},
                      'irradiance_2': {'type': 'irradiance', 'file': 'input_files/data/irradiance/Lindenberg2006BSRN_Irradiance_60sec.csv'},
                      'temperature_2': {'type': 'air_temperature', 'file': 'input_files/data/temperature/temperature.csv'},
                      'demand_electric_2': {'type': 'elec_demand', 'generate': {'yearly_demand': 3000}},
                      'demand_heat_2': {'type': 'therm_demand', 'generate': {'yearly_demand': 6000, 'temperature': 'temperature_2'}},
                      'demand_hot_water_2': {'type': 'hot_water_demand', 'generate': {'yearly_demand': 1500, 'temperature': 'temperature_2'}},
                      'irradiance_3': {'type': 'irradiance', 'file': 'input_files/data/irradiance/Lindenberg2006BSRN_Irradiance_60sec.csv'},
                      'temperature_3': {'type': 'air_temperature', 'file': 'input_files/data/temperature/temperature.csv'},
                      'demand_electric_3': {'type': 'elec_demand', 'generate': {'yearly_demand': 0}},
                      'demand_heat_3': {'type': 'therm_demand', 'generate': {'yearly_demand': 0, 'temperature': 'temperature_3'}},
                      'demand_hot_water_3': {'type': 'hot_water_demand', 'generate': {'yearly_demand': 0, 'temperature': 'temperature_3'}},
                      'elec_price_1': {'type': 'elec_price', 'file': 'input_files/data/prices/day-ahead/hourly_price.csv'}}

input_profiles = Tooling.input_profile_processor.input_profile_processor.process_input_profiles(input_profile_dict, t_start, dynamic)

prosumer_paths = {'SCN2_CAT1_PV11_3000_6000': 'input_files/models/prosumer_models/SCN2_CAT1_PV11/prosumer.json',
                  'SCN0_CAT1_3000_6000': 'input_files/models/prosumer_models/SCN0_CAT1/prosumer.json'}
prosumer_profiles = {'SCN2_CAT1_PV11_3000_6000': {'pv_roof': {'irradiance' : 'irradiance_1', 'temperature': 'temperature_1'},
                                                  'elec_cns': {'consumption': 'demand_electric_1'},
                                                  'therm_cns': {'consumption': 'demand_heat_1'},
                                                  'dhw_dmd': {'consumption': 'demand_hot_water_1'}},
                     'SCN0_CAT1_3000_6000': {'elec_cns': {'consumption': 'demand_electric_2'},
                                             'therm_cns': {'consumption': 'demand_heat_2'},
                                             'dhw_dmd': {'consumption': 'demand_hot_water_2'}}}
prosumer_dict = dict()
for prosumer_name, prosumer_path in prosumer_paths.items():
    with open(prosumer_path) as f:
        prosumer_json = json.load(f)
    prosumer_dict[prosumer_name] = prosumer_json
for prosumer_name, component_profiles in prosumer_profiles.items():
    for component_name, profiles in component_profiles.items():
        prosumer_dict[prosumer_name]['components'][component_name].update(profiles)

prosumer_main = main_prosumer.ProsumerMain(prosumer_dict, input_profiles, dynamic)

prosumer_sizing_strategy = ['annuity']
prosumer_main.optimize_sizing('sized', prosumer_sizing_strategy)

prosumer_main.save_results()

prosumers = prosumer_main.prosumers

if simulation_scope == SimulationScope.PROSUMER:
    exit()

district_asset_paths = {'ca_bat': 'input_files/models/district_models/example_CA/prosumer.json'}
district_asset_profiles = {'ca_bat': {'elec_cns': {'consumption': 'demand_electric_3'}}}
district_asset_dict = dict()
for district_asset_name, district_asset_path in district_asset_paths.items():
    with open(district_asset_path) as f:
        district_asset_json = json.load(f)
    district_asset_dict[district_asset_name] = district_asset_json
for district_asset_name, component_profiles in district_asset_profiles.items():
    for component_name, profiles in component_profiles.items():
        district_asset_dict[district_asset_name]['components'][component_name].update(profiles)

district_assets = main_prosumer.DistrictAssetMain(district_asset_dict, input_profiles, dynamic).district_assets

district_paths = {'community': 'input_files/models/district_models/example_community/district.json'}
district_profiles = {'community': {'wholesale_price': 'elec_price_1', 'injection_price': 'elec_price_1'}}
district_dict = dict()
for district_name, district_path in district_paths.items():
    with open(district_path) as f:
        district_json = json.load(f)
    district_dict[district_name] = district_json
for district_name, profiles in district_profiles.items():
    district_dict[district_name].update(profiles)

district_main = main_district.DistrictMain(district_dict, prosumers, district_assets, input_profiles, dynamic)

district_sizing_strategy = 'max_operational_profit'
district_main.optimize_sizing('sized', district_sizing_strategy)

district_operation_strategy = 'max_operational_profit'
district_main.optimize_operation('validated', district_operation_strategy)

district_main.save_results()
