.. FEN InEEd-DC documentation master file, created by
   sphinx-quickstart on Wed Sep 14 14:49:18 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FEN InEEd-DC's documentation!
========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Overview 9d9037b176744769a3404d91a14f4ee0.md
   Installation 004393c4782a4cf9b6bf8b7722ba7a9c.md
   Development ff4105c5c3a04acf898aa42fbe285afb.md
   Framework Structure 237a636d09a54d1592923b6127a04b8c.md
   module.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
