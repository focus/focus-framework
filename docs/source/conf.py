# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
import sys
import os

project = 'FOCUS'
copyright = '2022, FEN InEEd-DC Team'
author = 'FEN InEEd-DC Team'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

base_path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
model_path = os.path.join(base_path, 'Model_Library')
tool_path = os.path.join(base_path, 'Tooling')
prosumer_path = os.path.join(model_path, 'Prosumer', 'model')

sys.path.insert(0, base_path)
sys.path.insert(0, model_path)
sys.path.insert(0, tool_path)
sys.path.insert(0, prosumer_path)

extensions = ['sphinx_rtd_theme',
              'sphinx.ext.autodoc',
              'myst_parser']

templates_path = ['_templates']
exclude_patterns = []

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
